uniform texture2d spinda;
uniform texture2d spindaMask;
uniform texture2d spotTopLeft;
uniform texture2d spotTopRight;
uniform texture2d spotBottomLeft;
uniform texture2d spotBottomRight;

uniform float threshold<
  string label = "Threshold Adjustment";
  string widget_type = "slider";
  float minimum = 0.0;
  float maximum = 1.0;
  float step = 0.001;
> = 0.5;
uniform float resolutionWidth<
float minimum = 1; 
float maximum = 100000;
> = 1920.0;
uniform float resolutionHeight<
float minimum = 1; 
float maximum = 100000;
> = 1080.0;
uniform float downScale<
float minimum = 0.001; 
float maximum = 10.0;
> = 0.5;
uniform float weightBias<
float minimum = 0.01; 
float maximum = 1000.0;
> = 10.0;
uniform bool inverted = false;
uniform bool disable = true;

uniform int spriteWidth = 52;
uniform int spriteHeight = 59;
uniform int bodyHeight = 49;
uniform int headWidth = 37;
uniform int headHeight = 33;
uniform int headOverlapX = 10;
uniform int headOverlapY = 5;

uniform int spotTLX = 0;
uniform int spotTLY = 0;
uniform int spotTRX = 24;
uniform int spotTRY = 1;
uniform int spotBLX = 6;
uniform int spotBLY = 18;
uniform int spotBRX = 18;
uniform int spotBRY = 19;

float2 GetPixelOnScreen(float2 uv) {
	return float2(uv.x * resolutionWidth, uv.y * resolutionHeight);
}

float GrayScale(float4 color) {
	return color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
}

float BlackAndWhite(float4 color) {
	float gray = GrayScale(color);
	gray *= color.a;
	if (gray < threshold) {
		if (inverted) {
			return 1.0;
		} else {
			return 0.0;
		}
	} else {
		if (inverted) {
			return 0.0;
		} else {
			return 1.0;
		}
	}
}

float WhiteAndGray(float4 color) {
	float gray = GrayScale(color);
	gray *= color.a;
	if (gray < threshold) {
		if (inverted) {
			return 1.0;
		} else {
			return gray;
		}
	} else {
		if (inverted) {
			return gray;
		} else {
			return 1.0;
		}
	}
}

int2 SpindaCount() {
	return int2(resolutionWidth / (headWidth - headOverlapX), resolutionHeight / (headHeight - headOverlapY - 1));
}

float4 SpindaSample(int2 uv) {
	return spinda.Load(int3(uv.xy, 0));
}

float4 MaskSample(int2 uv) {
	return spinda.Load(int3(uv.xy, 0));
}

float4 ColorOverlay(float4 under, float4 over) {
	return float4(over.r * over.a + under.r * (1.0 - over.a), over.g * over.a + under.g * (1.0 - over.a), over.b * over.a + under.b * (1.0 - over.a), over.a + under.a * (1.0 - over.a));
}

bool InRect(int2 rPos, int2 size, int2 pos) {
	return (pos.x >= rPos.x && pos.x < size.x + rPos.x && pos.y >= rPos.y && pos.y < size.y + rPos.y);
}

float4 GetPixelColor(float2 uv, int2 screenPixel, texture2d screenTexture, int2 spindaPixel) {
	float4 pixelColor = float4(0.0, 0.0, 0.0, 1.0);
	float4 baseColor = SpindaSample(spindaPixel);
	float4 maskColor = MaskSample(spindaPixel);
	pixelColor = baseColor;
	
	if (maskColor.a <= 0.1) {
		return float4(1.0, 1.0, 1.0, 1.0);
	}
	//return pixelColor;
	
	bool inTL = InRect(int2(spotTLX, spotTLY), int2(28, 28), int2(spindaPixel));
	bool inTR = InRect(int2(spotTRX, spotTRY), int2(29, 29), int2(spindaPixel));
	bool inBL = InRect(int2(spotBLX, spotBLY), int2(24, 25), int2(spindaPixel));
	bool inBR = InRect(int2(spotBRX, spotBRY), int2(23, 25), int2(spindaPixel));
	
	int2 rectCornerTL = int2(spotTLX + (screenPixel.x - spindaPixel.x), spotTLY + (screenPixel.y - spindaPixel.y));
	int2 rectCornerTR = int2(spotTRX + (screenPixel.x - spindaPixel.x), spotTRY + (screenPixel.y - spindaPixel.y));
	int2 rectCornerBL = int2(spotBLX + (screenPixel.x - spindaPixel.x), spotBLY + (screenPixel.y - spindaPixel.y));
	int2 rectCornerBR = int2(spotBRX + (screenPixel.x - spindaPixel.x), spotBRY + (screenPixel.y - spindaPixel.y));
	
	int2 spotTLTotal = int2(0, 0);
	int2 spotTRTotal = int2(0, 0);
	int2 spotBLTotal = int2(0, 0);
	int2 spotBRTotal = int2(0, 0);
	
	int spotTLCount = 0;
	int spotTRCount = 0;
	int spotBLCount = 0;
	int spotBRCount = 0;
	
	int loopCountX = 0;
	int loopCountY = 0;
	
	if (inBL) {
		loopCountX = 23;
		loopCountY = 25;
	}
	
	if (inBR) {
		loopCountX = 24;
		loopCountY = 25;
	}
	
	if (inTL) {
		loopCountX = 28;
		loopCountY = 28;
	}
	
	if (inTR) {
		loopCountX = 29;
		loopCountY = 29;
	}
	
	for (int baseY = 0; baseY < loopCountY; baseY++) {
		for (int baseX = 0; baseX < loopCountX; baseX++) {
			if (inTL && baseX < 28 && baseY < 28) {
				int2 testPixel = int2(rectCornerTL.x + baseX, rectCornerTL.y + baseY) * (1 / downScale);
				float maskSample = MaskSample(int2(spotTLX + baseX, spotTLY + baseY)).a;
				float4 pixColor = screenTexture.Load(int3(testPixel.xy, 0));
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotTLTotal += int2(spotTLX + baseX, spotTLY + baseY);
						spotTLCount += 1;
					}
				}
			}
			
			if (inTR) {
				int2 testPixel = int2(rectCornerTR.x + baseX, rectCornerTR.y + baseY) * (1 / downScale);
				float maskSample = MaskSample(int2(spotTRX + baseX, spotTRY + baseY)).a;
				float4 pixColor = screenTexture.Load(int3(testPixel.xy, 0));
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotTRTotal += int2(spotTRX + baseX, spotTRY + baseY);
						spotTRCount += 1;
					}
				}
			}
			
			if (inBL && baseX < 22 && baseY < 24) {
				int2 testPixel = int2(rectCornerBL.x + baseX, rectCornerBL.y + baseY) * (1 / downScale);
				float maskSample = MaskSample(int2(spotBLX + baseX, spotBLY + baseY)).a;
				float4 pixColor = screenTexture.Load(int3(testPixel.xy, 0));
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotBLTotal += int2(spotBLX + baseX, spotBLY + baseY);
						spotBLCount += 1;
					}
				}
			}
			
			if (inBR && baseX < 23 && baseY < 24) {
				int2 testPixel = int2(rectCornerBR.x + baseX, rectCornerBR.y + baseY) * (1 / downScale);
				float maskSample = MaskSample(int2(spotBRX + baseX, spotBRY + baseY)).a;
				float4 pixColor = screenTexture.Load(int3(testPixel.xy, 0));
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotBRTotal += int2(spotBRX + baseX, spotBRY + baseY);
						spotBRCount += 1;
					}
				}
			}
		}
	}
	
	if (spotTLCount <= 0) {
		spotTLTotal = int2(spotTLX, spotTLY);
		spotTLCount = 1;
	}
	
	if (spotTRCount <= 0) {
		spotTRTotal = int2(spotTRX + 25, spotTRY);
		spotTRCount = 1;
	}
	
	if (spotBLCount <= 0) {
		spotBLTotal = int2(spotBLX, spotBLY + 25);
		spotBLCount = 1;
	}
	
	if (spotBRCount <= 0) {
		spotBRTotal = int2(spotBRX + 25, spotBRY + 25);
		spotBRCount = 1;
	}
	
	spotTLTotal /= spotTLCount;
	spotTRTotal /= spotTRCount;
	spotBLTotal /= spotBLCount;
	spotBRTotal /= spotBRCount;
	
	spotTLTotal = int2(clamp(spotTLTotal.x - 6, spotTLX, spotTLX + 15), clamp(spotTLTotal.y - 6, spotTLY, spotTLY + 15));
	spotTRTotal = int2(clamp(spotTRTotal.x - 6, spotTRX, spotTRX + 15), clamp(spotTRTotal.y - 6, spotTRY, spotTRY + 15));
	spotBLTotal = int2(clamp(spotBLTotal.x - 3, spotBLX, spotBLX + 15), clamp(spotBLTotal.y - 4, spotBLY, spotBLY + 15));
	spotBRTotal = int2(clamp(spotBRTotal.x - 4, spotBRX, spotBRX + 15), clamp(spotBRTotal.y - 4, spotBRY, spotBRY + 15));
	
	if (InRect(spotTLTotal, int2(12, 12), spindaPixel)) {
		int2 spotSample = spindaPixel - spotTLTotal;
		float4 spotColor = spotTopLeft.Load(int3(spotSample.xy, 0));
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	if (InRect(spotTRTotal, int2(13, 13), spindaPixel)) {
		int2 spotSample = spindaPixel - spotTRTotal;
		float4 spotColor = spotTopRight.Load(int3(spotSample.xy, 0));
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	if (InRect(spotBLTotal, int2(7, 9), spindaPixel)) {
		int2 spotSample = spindaPixel - spotBLTotal;
		float4 spotColor = spotBottomLeft.Load(int3(spotSample.xy, 0));
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	if (InRect(spotBRTotal, int2(8, 9), spindaPixel)) {
		int2 spotSample = spindaPixel - spotBRTotal;
		float4 spotColor = spotBottomRight.Load(int3(spotSample.xy, 0));
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	return pixelColor;
}

float4 GetColor(texture2d screenTexture, float2 uv) {
	float4 pixelColor = float4(0.0, 0.0, 0.0, 0.0);
	int2 pixel = GetPixelOnScreen(uv);
	int2 spindas = SpindaCount();
	
	int totalOverlapY = headOverlapY + (headHeight - (bodyHeight - headHeight));
	
	if (pixel.x < 0 || pixel.y < 0) return float4(0.0, 0.0, 0.0, 0.0);
	if (pixel.x >= spindas.x * (headWidth - headOverlapX) + headOverlapX) return float4(0.0, 0.0, 0.0, 0.0);
	if (pixel.y >= spindas.y * (totalOverlapY + headOverlapY) - headOverlapY) return float4(0.0, 0.0, 0.0, 0.0);

	float4 bottomColor = float4(0.0, 0.0, 0.0, 0.0);
	float4 topColor = float4(0.0, 0.0, 0.0, 0.0);
	
	int checkWidth = headWidth - headOverlapX;
	int pixelPosX = pixel.x % checkWidth;
	int inSpindaX = pixel.x / checkWidth;
	
	int checkHeight = totalOverlapY + headOverlapY;
	int pixelPosY = pixel.y % checkHeight;
	int inSpindaY = pixel.y / checkHeight;
	
	if (pixelPosY < totalOverlapY && pixel.y > totalOverlapY && inSpindaY < spindas.y - 1) {
		//pixelColor = ColorOverlay(pixelColor, float4(0.0, 0.0, 1.0, 0.5));
		if (pixelPosX < headOverlapX && pixel.x > headOverlapX && inSpindaX < spindas.x) {
			int2 spindaPixelL = int2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			int2 spindaPixelR = int2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			float4 pixelColorL = (MaskSample(spindaPixelL).a > 0.1 && SpindaSample(spindaPixelR).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL) : SpindaSample(spindaPixelL);
			float4 pixelColorR = MaskSample(spindaPixelR).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR) : SpindaSample(spindaPixelR);
			bottomColor = ColorOverlay(pixelColorL, pixelColorR);
			
			int2 spindaPixelL2 = int2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			int2 spindaPixelR2 = int2(pixelPosX + 8, pixelPosY + 8);
			float4 pixelColorL2 = (MaskSample(spindaPixelL2).a > 0.1 && SpindaSample(spindaPixelR2).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL2) : SpindaSample(spindaPixelL2);
			float4 pixelColorR2 = MaskSample(spindaPixelR2).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR2) : SpindaSample(spindaPixelR2);
			topColor = ColorOverlay(pixelColorL2, pixelColorR2);
		} else if (pixelPosX < headOverlapX && pixel.x > headOverlapX) {
			int2 topPixel = int2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
			
			int2 bottomPixel = int2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			bottomColor = MaskSample(bottomPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, bottomPixel) : SpindaSample(bottomPixel);
		} else {
			int2 topPixel = int2(pixelPosX + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
			
			int2 bottomPixel = int2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			bottomColor = MaskSample(bottomPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, bottomPixel) : SpindaSample(bottomPixel);
		}
		
	} else if (pixelPosY < totalOverlapY && pixel.y > totalOverlapY) {
		if (pixelPosX < headOverlapX && pixel.x > headOverlapX && inSpindaX < spindas.x) {
			int2 spindaPixelL = int2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			int2 spindaPixelR = int2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			float4 pixelColorL = (MaskSample(spindaPixelL).a > 0.1 && SpindaSample(spindaPixelR).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL) : SpindaSample(spindaPixelL);
			float4 pixelColorR = MaskSample(spindaPixelR).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR) : SpindaSample(spindaPixelR);
			topColor = ColorOverlay(pixelColorL, pixelColorR);
		} else if (pixelPosX < headOverlapX && pixel.x > headOverlapX) {
			int2 topPixel = int2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		} else {
			int2 topPixel = int2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		}
	}  else {
		if (pixelPosX < headOverlapX && pixel.x > headOverlapX && inSpindaX < spindas.x) {
			int2 spindaPixelL = int2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			int2 spindaPixelR = int2(pixelPosX + 8, pixelPosY + 8);
			float4 pixelColorL = (MaskSample(spindaPixelL).a > 0.1 && SpindaSample(spindaPixelR).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL) : SpindaSample(spindaPixelL);
			float4 pixelColorR = MaskSample(spindaPixelR).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR) : SpindaSample(spindaPixelR);
			topColor = ColorOverlay(pixelColorL, pixelColorR);
		} else if (pixelPosX < headOverlapX && pixel.x > headOverlapX) {
			int2 topPixel = int2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		} else {
			int2 topPixel = int2(pixelPosX + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		}
	}
	
	pixelColor = ColorOverlay(ColorOverlay(bottomColor, topColor), pixelColor);
	
	return pixelColor;
}

float4 mainImage(VertData v_in) : TARGET
{
	float2 scaledUV = v_in.uv * (1.0 / downScale);
	if (!disable) {
		return GetColor(image, scaledUV);
	} else {
		int2 pixel = GetPixelOnScreen(scaledUV);
		return float4(WhiteAndGray(image.Load(int3(pixel.xy, 0) * (1 / downScale))), WhiteAndGray(image.Load(int3(pixel.xy, 0) * (1 / downScale))), WhiteAndGray(image.Load(int3(pixel.xy, 0) * (1 / downScale))), 1.0);
	}
}
