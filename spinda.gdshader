shader_type canvas_item;
uniform sampler2D screen_texture : hint_screen_texture, repeat_disable, filter_nearest;

uniform sampler2D spinda;
uniform sampler2D spindaMask;
uniform sampler2D spotTopLeft;
uniform sampler2D spotTopRight;
uniform sampler2D spotBottomLeft;
uniform sampler2D spotBottomRight;

uniform float threshold = 0.5;
uniform int resolutionWidth = 1920;
uniform int resolutionHeight = 1080;
uniform float downScale = 0.5;
uniform float weightBias = 10.0;

uniform bool inverted = false;
uniform bool disable = false;

uniform int spriteWidth = 52;
uniform int spriteHeight = 59;
uniform int bodyHeight = 49;
uniform int headWidth = 37;
uniform int headHeight = 33;
uniform int headOverlapX = 10;
uniform int headOverlapY = 5;

uniform int spotTLX = 0;
uniform int spotTLY = 0;
uniform int spotTRX = 24;
uniform int spotTRY = 1;
uniform int spotBLX = 6;
uniform int spotBLY = 18;
uniform int spotBRX = 18;
uniform int spotBRY = 19;

ivec2 GetPixelOnScreen(vec2 uv) {
	return ivec2(int(uv.x * float(resolutionWidth)), int(uv.y * float(resolutionHeight)));
}

float GrayScale(vec4 color) {
	return color.r * 0.2126 + color.g * 0.7152 + color.b * 0.0722;
}

float BlackAndWhite(vec4 color) {
	float gray = GrayScale(color);
	gray *= color.a;
	if (gray < threshold) {
		if (inverted) {
			return 1.0;
		} else {
			return 0.0;
		}
	} else {
		if (inverted) {
			return 0.0;
		} else {
			return 1.0;
		}
	}
}

float WhiteAndGray(vec4 color) {
	float gray = GrayScale(color);
	gray *= color.a;
	if (gray < threshold) {
		if (inverted) {
			return 1.0;
		} else {
			return gray;
		}
	} else {
		if (inverted) {
			return gray;
		} else {
			return 1.0;
		}
	}
}

ivec2 SpindaCount() {
	return ivec2(resolutionWidth / (headWidth - headOverlapX), resolutionHeight / (headHeight - headOverlapY - 1));
}

vec4 SpindaSample(ivec2 uv) {
	//return spinda.SampleLevel(textureSampler, uv, 0);
	return texelFetch(spinda, uv, 0);
}

vec4 MaskSample(ivec2 uv) {
	//return spinda.SampleLevel(textureSampler, uv, 0);
	return texelFetch(spindaMask, uv, 0);
}

vec4 ColorOverlay(vec4 under, vec4 over) {
	return vec4(over.r * over.a + under.r * (1.0 - over.a), over.g * over.a + under.g * (1.0 - over.a), over.b * over.a + under.b * (1.0 - over.a), over.a + under.a * (1.0 - over.a));
}

bool InRect(ivec2 rPos, ivec2 size, ivec2 pos) {
	return (pos.x >= rPos.x && pos.x < size.x + rPos.x && pos.y >= rPos.y && pos.y < size.y + rPos.y);
}

vec4 GetPixelColor(vec2 uv, ivec2 screenPixel, sampler2D screenTexture, ivec2 spindaPixel) {
	vec4 pixelColor = vec4(0.0, 0.0, 0.0, 1.0);
	vec4 baseColor = SpindaSample(spindaPixel);
	vec4 maskColor = MaskSample(spindaPixel);
	pixelColor = baseColor;
	
	if (maskColor.a <= 0.1) {
		return vec4(1.0, 1.0, 1.0, 1.0);
	}
	//return pixelColor;
	
	bool inTL = InRect(ivec2(spotTLX, spotTLY), ivec2(28, 28), ivec2(spindaPixel));
	bool inTR = InRect(ivec2(spotTRX, spotTRY), ivec2(29, 29), ivec2(spindaPixel));
	bool inBL = InRect(ivec2(spotBLX, spotBLY), ivec2(24, 25), ivec2(spindaPixel));
	bool inBR = InRect(ivec2(spotBRX, spotBRY), ivec2(23, 25), ivec2(spindaPixel));
	
	ivec2 rectCornerTL = ivec2(spotTLX + (screenPixel.x - spindaPixel.x), spotTLY + (screenPixel.y - spindaPixel.y));
	ivec2 rectCornerTR = ivec2(spotTRX + (screenPixel.x - spindaPixel.x), spotTRY + (screenPixel.y - spindaPixel.y));
	ivec2 rectCornerBL = ivec2(spotBLX + (screenPixel.x - spindaPixel.x), spotBLY + (screenPixel.y - spindaPixel.y));
	ivec2 rectCornerBR = ivec2(spotBRX + (screenPixel.x - spindaPixel.x), spotBRY + (screenPixel.y - spindaPixel.y));
	
	ivec2 spotTLTotal = ivec2(0, 0);
	ivec2 spotTRTotal = ivec2(0, 0);
	ivec2 spotBLTotal = ivec2(0, 0);
	ivec2 spotBRTotal = ivec2(0, 0);
	
	int spotTLCount = 0;
	int spotTRCount = 0;
	int spotBLCount = 0;
	int spotBRCount = 0;
	
	int loopCountX = 0;
	int loopCountY = 0;
	
	if (inBL) {
		loopCountX = 23;
		loopCountY = 25;
	}
	
	if (inBR) {
		loopCountX = 24;
		loopCountY = 25;
	}
	
	if (inTL) {
		loopCountX = 28;
		loopCountY = 28;
	}
	
	if (inTR) {
		loopCountX = 29;
		loopCountY = 29;
	}
	
	for (int baseY = 0; baseY < loopCountY; baseY++) {
		for (int baseX = 0; baseX < loopCountX; baseX++) {
			if (inTL && baseX < 28 && baseY < 28) {
				ivec2 testPixel = ivec2(rectCornerTL.x + baseX, rectCornerTL.y + baseY);
				float maskSample = MaskSample(ivec2(spotTLX + baseX, spotTLY + baseY)).a;
				vec4 pixColor = texelFetch(screenTexture, testPixel, 0);
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotTLTotal += ivec2(spotTLX + baseX, spotTLY + baseY);
						spotTLCount += 1;
					}
				}
			}
			
			if (inTR) {
				ivec2 testPixel = ivec2(rectCornerTR.x + baseX, rectCornerTR.y + baseY);
				float maskSample = MaskSample(ivec2(spotTRX + baseX, spotTRY + baseY)).a;
				vec4 pixColor = texelFetch(screenTexture, testPixel, 0);
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotTRTotal += ivec2(spotTRX + baseX, spotTRY + baseY);
						spotTRCount += 1;
					}
				}
			}
			
			if (inBL && baseX < 22 && baseY < 24) {
				ivec2 testPixel = ivec2(rectCornerBL.x + baseX, rectCornerBL.y + baseY);
				float maskSample = MaskSample(ivec2(spotBLX + baseX, spotBLY + baseY)).a;
				vec4 pixColor = texelFetch(screenTexture, testPixel, 0);
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotBLTotal += ivec2(spotBLX + baseX, spotBLY + baseY);
						spotBLCount += 1;
					}
				}
			}
			
			if (inBR && baseX < 23 && baseY < 24) {
				ivec2 testPixel = ivec2(rectCornerBR.x + baseX, rectCornerBR.y + baseY);
				float maskSample = MaskSample(ivec2(spotBRX + baseX, spotBRY + baseY)).a;
				vec4 pixColor = texelFetch(screenTexture, testPixel, 0);
				
				if (BlackAndWhite(pixColor) <= threshold && maskSample >= 0.1) {
					for (float weight = threshold - WhiteAndGray(pixColor); weight < threshold; weight += threshold / weightBias) {
						spotBRTotal += ivec2(spotBRX + baseX, spotBRY + baseY);
						spotBRCount += 1;
					}
				}
			}
		}
	}
	
	if (spotTLCount <= 0) {
		spotTLTotal = ivec2(spotTLX, spotTLY);
		spotTLCount = 1;
	}
	
	if (spotTRCount <= 0) {
		spotTRTotal = ivec2(spotTRX + 25, spotTRY);
		spotTRCount = 1;
	}
	
	if (spotBLCount <= 0) {
		spotBLTotal = ivec2(spotBLX, spotBLY + 25);
		spotBLCount = 1;
	}
	
	if (spotBRCount <= 0) {
		spotBRTotal = ivec2(spotBRX + 25, spotBRY + 25);
		spotBRCount = 1;
	}
	
	spotTLTotal /= spotTLCount;
	spotTRTotal /= spotTRCount;
	spotBLTotal /= spotBLCount;
	spotBRTotal /= spotBRCount;
	
	spotTLTotal = ivec2(clamp(spotTLTotal.x - 6, spotTLX, spotTLX + 15), clamp(spotTLTotal.y - 6, spotTLY, spotTLY + 15));
	spotTRTotal = ivec2(clamp(spotTRTotal.x - 6, spotTRX, spotTRX + 15), clamp(spotTRTotal.y - 6, spotTRY, spotTRY + 15));
	spotBLTotal = ivec2(clamp(spotBLTotal.x - 3, spotBLX, spotBLX + 15), clamp(spotBLTotal.y - 4, spotBLY, spotBLY + 15));
	spotBRTotal = ivec2(clamp(spotBRTotal.x - 4, spotBRX, spotBRX + 15), clamp(spotBRTotal.y - 4, spotBRY, spotBRY + 15));
	
	if (InRect(spotTLTotal, ivec2(12, 12), spindaPixel)) {
		ivec2 spotSample = spindaPixel - spotTLTotal;
		vec4 spotColor = texelFetch(spotTopLeft, spotSample, 0);
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	if (InRect(spotTRTotal, ivec2(13, 13), spindaPixel)) {
		ivec2 spotSample = spindaPixel - spotTRTotal;
		vec4 spotColor = texelFetch(spotTopRight, spotSample, 0);
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	if (InRect(spotBLTotal, ivec2(7, 9), spindaPixel)) {
		ivec2 spotSample = spindaPixel - spotBLTotal;
		vec4 spotColor = texelFetch(spotBottomLeft, spotSample, 0);
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	if (InRect(spotBRTotal, ivec2(8, 9), spindaPixel)) {
		ivec2 spotSample = spindaPixel - spotBRTotal;
		vec4 spotColor = texelFetch(spotBottomRight, spotSample, 0);
		float maskColor = MaskSample(spindaPixel).a;
		spotColor.a *= maskColor;
		pixelColor = ColorOverlay(pixelColor, spotColor);
	}
	
	return pixelColor;
}

vec4 GetColor(sampler2D screenTexture, vec2 uv) {
	vec4 pixelColor = vec4(0.0, 0.0, 0.0, 0.0);
	ivec2 pixel = GetPixelOnScreen(uv);
	ivec2 spindas = SpindaCount();
	
	int totalOverlapY = headOverlapY + (headHeight - (bodyHeight - headHeight));
	
	if (pixel.x < 0 || pixel.y < 0) return vec4(0.0, 0.0, 0.0, 0.0);
	if (pixel.x >= spindas.x * (headWidth - headOverlapX) + headOverlapX) return vec4(0.0, 0.0, 0.0, 0.0);
	if (pixel.y >= spindas.y * (totalOverlapY + headOverlapY) - headOverlapY) return vec4(0.0, 0.0, 0.0, 0.0);

	vec4 bottomColor = vec4(0.0, 0.0, 0.0, 0.0);
	vec4 topColor = vec4(0.0, 0.0, 0.0, 0.0);
	
	int checkWidth = headWidth - headOverlapX;
	int pixelPosX = int(pixel.x) % checkWidth;
	int inSpindaX = pixel.x / checkWidth;
	
	int checkHeight = totalOverlapY + headOverlapY;
	int pixelPosY = int(pixel.y) % checkHeight;
	int inSpindaY = pixel.y / checkHeight;
	
	if (pixelPosY < totalOverlapY && pixel.y > totalOverlapY && inSpindaY < spindas.y - 1) {
		//pixelColor = ColorOverlay(pixelColor, vec4(0.0, 0.0, 1.0, 0.5));
		if (pixelPosX < headOverlapX && pixel.x > headOverlapX && inSpindaX < spindas.x) {
			ivec2 spindaPixelL = ivec2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			ivec2 spindaPixelR = ivec2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			vec4 pixelColorL = (MaskSample(spindaPixelL).a > 0.1 && SpindaSample(spindaPixelR).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL) : SpindaSample(spindaPixelL);
			vec4 pixelColorR = MaskSample(spindaPixelR).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR) : SpindaSample(spindaPixelR);
			bottomColor = ColorOverlay(pixelColorL, pixelColorR);
			
			ivec2 spindaPixelL2 = ivec2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			ivec2 spindaPixelR2 = ivec2(pixelPosX + 8, pixelPosY + 8);
			vec4 pixelColorL2 = (MaskSample(spindaPixelL2).a > 0.1 && SpindaSample(spindaPixelR2).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL2) : SpindaSample(spindaPixelL2);
			vec4 pixelColorR2 = MaskSample(spindaPixelR2).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR2) : SpindaSample(spindaPixelR2);
			topColor = ColorOverlay(pixelColorL2, pixelColorR2);
		} else if (pixelPosX < headOverlapX && pixel.x > headOverlapX) {
			ivec2 topPixel = ivec2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
			
			ivec2 bottomPixel = ivec2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			bottomColor = MaskSample(bottomPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, bottomPixel) : SpindaSample(bottomPixel);
		} else {
			ivec2 topPixel = ivec2(pixelPosX + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
			
			ivec2 bottomPixel = ivec2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			bottomColor = MaskSample(bottomPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, bottomPixel) : SpindaSample(bottomPixel);
		}
		
	} else if (pixelPosY < totalOverlapY && pixel.y > totalOverlapY) {
		if (pixelPosX < headOverlapX && pixel.x > headOverlapX && inSpindaX < spindas.x) {
			ivec2 spindaPixelL = ivec2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			ivec2 spindaPixelR = ivec2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			vec4 pixelColorL = (MaskSample(spindaPixelL).a > 0.1 && SpindaSample(spindaPixelR).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL) : SpindaSample(spindaPixelL);
			vec4 pixelColorR = MaskSample(spindaPixelR).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR) : SpindaSample(spindaPixelR);
			topColor = ColorOverlay(pixelColorL, pixelColorR);
		} else if (pixelPosX < headOverlapX && pixel.x > headOverlapX) {
			ivec2 topPixel = ivec2(pixelPosX + checkWidth + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		} else {
			ivec2 topPixel = ivec2(pixelPosX + 8, pixelPosY + totalOverlapY + headOverlapY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		}
	}  else {
		if (pixelPosX < headOverlapX && pixel.x > headOverlapX && inSpindaX < spindas.x) {
			ivec2 spindaPixelL = ivec2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			ivec2 spindaPixelR = ivec2(pixelPosX + 8, pixelPosY + 8);
			vec4 pixelColorL = (MaskSample(spindaPixelL).a > 0.1 && SpindaSample(spindaPixelR).a <= 0.1) ? GetPixelColor(uv, pixel, screenTexture, spindaPixelL) : SpindaSample(spindaPixelL);
			vec4 pixelColorR = MaskSample(spindaPixelR).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, spindaPixelR) : SpindaSample(spindaPixelR);
			topColor = ColorOverlay(pixelColorL, pixelColorR);
		} else if (pixelPosX < headOverlapX && pixel.x > headOverlapX) {
			ivec2 topPixel = ivec2(pixelPosX + checkWidth + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		} else {
			ivec2 topPixel = ivec2(pixelPosX + 8, pixelPosY + 8);
			topColor = MaskSample(topPixel).a > 0.1 ? GetPixelColor(uv, pixel, screenTexture, topPixel) : SpindaSample(topPixel);
		}
	}
	
	pixelColor = ColorOverlay(ColorOverlay(bottomColor, topColor), pixelColor);
	
	return pixelColor;
}

void fragment() {
	vec2 scaledUV = UV * (1.0 / downScale);
	if (!disable) {
		vec4 col = GetColor(screen_texture, scaledUV);
    	COLOR = col;
	} else {
		ivec2 pixel = GetPixelOnScreen(scaledUV);
		COLOR = vec4(WhiteAndGray(texelFetch(screen_texture, pixel, 0)), WhiteAndGray(texelFetch(screen_texture, pixel, 0)), WhiteAndGray(texelFetch(screen_texture, pixel, 0)), 1.0);
		//COLOR = textureLod(screen_texture, UV, 0.0);
	}
}
